import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { validate as isUUID } from 'uuid';

import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { PaginationDto } from '../common/dto/pagination.dto';
import { ProductImage } from './entities/product-image.entity';
import { User } from '../auth/entities/user.entity';

@Injectable()
export class ProductsService {
  private readonly logger = new Logger(ProductsService.name);
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(ProductImage)
    private readonly productImageRepository: Repository<ProductImage>,

    private readonly dataSource: DataSource,
  ) {
    this.logger;
  }
  async create(createProductDto: CreateProductDto, user: User) {
    try {
      const { images = [], ...productDetail } = createProductDto;
      const product = await this.productRepository.create({
        ...productDetail,
        images: images.map((img) =>
          this.productImageRepository.create({ url: img }),
        ),
        user,
      });

      await this.productRepository.save(product);

      return { ...product, images };
    } catch (error) {
      this.handleDBExceptions(error);
    }
  }

  async findAll(paginationDto?: PaginationDto) {
    const { limit = 10, offset = 0 } = paginationDto;
    const products = await this.productRepository.find({
      take: limit,
      skip: offset,
      relations: {
        images: true,
      },
    });

    return products.map(({ images, ...rest }) => ({
      ...rest,
      images: images.map((img) => img.url),
    }));
  }

  async findOne(term: string): Promise<Product> {
    let product: Product;
    try {
      if (isUUID(term)) {
        product = await this.productRepository.findOneBy({
          id: term,
        });
      } else {
        const queryBuilder =
          this.productRepository.createQueryBuilder('product');

        product = await queryBuilder
          .where(`title =:title or slug=:slug`, {
            title: term,
            slug: term,
          })
          .leftJoinAndSelect('product.images', 'productImages')
          .getOne();
      }

      if (!product) {
        throw new NotFoundException(`Product with id "${term}" not found`);
      }
      return product;
    } catch (err) {
      this.handleDBExceptions(err);
    }
  }

  async findOnePlain(term: string) {
    const product = await this.findOne(term);

    const { images = [], ...rest } = product;
    return { ...rest, images: images.map((img) => img.url) };
  }

  async update(id: string, updateProductDto: UpdateProductDto, user: User) {
    const { images, ...productDetail } = updateProductDto;

    const product = await this.productRepository.preload({
      id: id,
      ...productDetail,
    });

    if (!product) {
      throw new NotFoundException(`Product with id "${id}" not found`);
    }

    //Create queryRunner
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      if (images) {
        queryRunner.manager.delete(ProductImage, {
          product: { id },
        });

        product.images = images.map((img) =>
          this.productImageRepository.create({ url: img }),
        );
      } else {
        product.images = await this.productImageRepository.findBy({
          product: { id: product.id },
        });
      }
      product.user = user;
      await queryRunner.manager.save(product);
      // return this.productRepository.save(product);
      await queryRunner.commitTransaction();
      await queryRunner.release();

      return this.findOnePlain(id);
    } catch (error) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();
      this.handleDBExceptions(error);
    }
  }

  async remove(id: string) {
    const productDB = await this.findOne(id);
    return await this.productRepository.remove(productDB);
  }

  private handleDBExceptions(error: any) {
    console.log(error);

    if (error.code === '23505' || error.response?.statusCode === 404)
      throw new BadRequestException(error.message);

    this.logger.error(error);

    throw new InternalServerErrorException(
      'Unexpected error, check server logs',
    );
  }

  async deleteAllProducts() {
    const queryBuilder = this.productRepository.createQueryBuilder('product');
    try {
      return await queryBuilder.delete().where({}).execute();
    } catch (e) {
      this.handleDBExceptions(e);
    }
  }
}
