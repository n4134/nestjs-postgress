export const fileFilter = (
  req: Express.Request,
  file: Express.Multer.File,
  callback,
) => {
  if (!file) return callback(new Error('File is empty'), false);

  const fileExptension = file.mimetype.split('/')[1];
  const validExtentions = ['jpg', 'jpeg', 'png', 'gif'];

  if (validExtentions.includes(fileExptension.toLowerCase()))
    callback(null, true);

  callback(null, false);
};
